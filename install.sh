#!/bin/bash
chmod u+x ./install/discord.sh
chmod u+x ./install/code.sh
chmod u+x ./install/python.sh
chmod u+x ./install/docker.sh
chmod u+x ./install/java.sh
chmod u+x ./install/cSharp.sh
chmod u+x ./install/javaFX.sh
chmod u+x ./install/mariaDB.sh
chmod u+x ./install/Flask.sh
chmod u+x ./test/test.sh
if test $1
then
  lieux=$1
else
  lieux=$(pwd)
fi
sudo apt update -y
./install/discord.sh
./install/code.sh
./install/python.sh
./install/java.sh
./install/cSharp.sh
./install/javaFX.sh
./install/mariaDB.sh
./install/Flask.sh
./install/docker.sh $lieux
read -p "Effectuer les Tests? Y/N" valider
if [[ "$valider" == [yY] || "$valider" == [yY][eE][sS] ]];
then
  ./test/test.sh
fi
