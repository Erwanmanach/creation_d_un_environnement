# Sae Bash Erwan Manach

- Vous trouverez ci joint tous les nécessaire pour l'installation de l'environnement
- Le lien vers le dépot gitlab https://gitlab.com/Erwanmanach/creation_d_un_environnement/-/tree/main

## Comment reproduire l'installation

Il faut pour faire l'installation:
- Disposez de au moins 20 Go pour assurez l'installation et la récupération de tous les fichiers:
Alors deux choix s'offre à vous soit utiliser une vm alors il faut cliquer sur le fichier .iso pour le faire monter puis effectuer l'installation sinon l'installer directement à partir d'une clé usb ou en pxe.
- Sur ce votre environnement il faut ensuite téléchager les différents fichiers pour l'installationa sinon en vm.
- Rentrez dans le répertoire d'installation et entrez dans un terminal à partir de cette endroit. 
- Effectuer la commande dans le terminal chmod u+x install.sh
- Effectuer ensuite la commande ./install.sh x  x doit remplacer par le chemin vous pour l'installation du docker et de ses répertoires sinon le fichier d'installation sera choisit par défaut.
- Si vous souhaitez vérifier la bonne installation un fichier est à cette disposition. Faite ./test.sh 

## Les différents choix effectué
- Le choix d'un fichier .iso a pour objectif de permettre l'installation par quasiment n'importe quel moyen.
- Un des premiers choix est la segmentation en plusieurs fichiers en différentes entités définis et claire pour permettre la séparation, la maintenance et la réutilisation de chaque partis plus facile.
- Code à été choisit pour permettre une plus grande flexibilité , utilisation de plusieurs langages et de pouvoir rajouter des extension facilement.
- Discord a été choisit pour permettre une communication simplifier
- Ils sont installés par snap pour faciliter leurs installations.
- Les différente extension pour code sont ajouté par la librairie de Code.
- Le reste est installé par apt.
- Les extension jdk pour permettre l'éxecution du java son obligatoire.

## les différentes sources
- https://ubuntu.com/download/desktop pour permettre de récuperer l'iso pour l'installation
- le forum de ubuntu
