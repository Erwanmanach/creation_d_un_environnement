#!/bin/bash
mkdir -p $1/ORACLE
mkdir -p $1/ORACLE/workspace
cd $1/ORACLE
sudo dpkg --configure -a
sudo apt update
sudo apt -y install docker.io
sudo usermod -aG docker $USER
cd -
newgrp docker
